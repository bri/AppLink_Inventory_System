﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppLink.Payroll.Domain.Entities;

namespace AppLink.Payroll.Controllers
{
    public class EDPController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: EDP
        public ActionResult Index()
        {
            var employeeDepartmentPositions = db.EmployeeDepartmentPositions.Include(e => e.Department).Include(e => e.Employee).Include(e => e.Position);
            return View(employeeDepartmentPositions.ToList());
        }

        // GET: EDP/Details/5
        public ActionResult Details(int? depId, int? empId, int? posId)
        {
       //TODO: modify this
            if (depId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeDepartmentPosition employeeDepartmentPosition = db.EmployeeDepartmentPositions.Find(empId,depId,posId);
            if (employeeDepartmentPosition == null)
            {
                return HttpNotFound();
            }
            return View(employeeDepartmentPosition);
        }

        // GET: EDP/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name");
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "EmployeeNumber");
            ViewBag.PositionId = new SelectList(db.Positions, "Id", "Name");
            return View();
        }

        // POST: EDP/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmployeeId,DepartmentId,PositionId,DateAdded")] EmployeeDepartmentPosition employeeDepartmentPosition)
        {
            if (ModelState.IsValid)
            {
                db.EmployeeDepartmentPositions.Add(employeeDepartmentPosition);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", employeeDepartmentPosition.DepartmentId);
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "EmployeeNumber", employeeDepartmentPosition.EmployeeId);
            ViewBag.PositionId = new SelectList(db.Positions, "Id", "Name", employeeDepartmentPosition.PositionId);
            return View(employeeDepartmentPosition);
        }

        // GET: EDP/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeDepartmentPosition employeeDepartmentPosition = db.EmployeeDepartmentPositions.Find(id);
            if (employeeDepartmentPosition == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", employeeDepartmentPosition.DepartmentId);
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "EmployeeNumber", employeeDepartmentPosition.EmployeeId);
            ViewBag.PositionId = new SelectList(db.Positions, "Id", "Name", employeeDepartmentPosition.PositionId);
            return View(employeeDepartmentPosition);
        }

        // POST: EDP/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmployeeId,DepartmentId,PositionId,DateAdded")] EmployeeDepartmentPosition employeeDepartmentPosition)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employeeDepartmentPosition).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", employeeDepartmentPosition.DepartmentId);
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "EmployeeNumber", employeeDepartmentPosition.EmployeeId);
            ViewBag.PositionId = new SelectList(db.Positions, "Id", "Name", employeeDepartmentPosition.PositionId);
            return View(employeeDepartmentPosition);
        }

        // GET: EDP/Delete/5
        public ActionResult Delete(int? depId, int? empId, int? posId)
        {
            if (depId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeDepartmentPosition employeeDepartmentPosition = db.EmployeeDepartmentPositions.Find(empId, depId, posId);
            if (employeeDepartmentPosition == null)
            {
                return HttpNotFound();
            }
            return View(employeeDepartmentPosition);
        }

        // POST: EDP/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? depId, int? empId, int? posId)
        {
            EmployeeDepartmentPosition employeeDepartmentPosition = db.EmployeeDepartmentPositions.Find(empId, depId, posId);
            db.EmployeeDepartmentPositions.Remove(employeeDepartmentPosition);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
