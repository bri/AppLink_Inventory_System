﻿using AppLink.Payroll.Controllers;
using AppLink.Payroll.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppLink.Payroll.Models
{
    public class ComputeSalaryViewModel
    {
        public PaySlip PaySlip { get; set; }
        public EmployeeDepartmentPosition EDP { get; set; }

        public DateTime TimeIn { get; set; }
        public DateTime TimeOut { get; set; }
    }
}