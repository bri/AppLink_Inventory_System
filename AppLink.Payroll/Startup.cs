﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AppLink.Payroll.Startup))]
namespace AppLink.Payroll
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
