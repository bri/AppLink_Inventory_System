﻿using AppLink.Payroll.Controllers;
using AppLink.Payroll.Domain.Abstract;
using AppLink.Payroll.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLink.Payroll.Tests.Controllers
{
    [TestClass]
    public class DepartmentControllerTest
    {

        [TestMethod]
        public void Can_Delete_Department()
        {
            var dep = new Department { Id = 3, Name = "Department test" };
            Mock<IPayrollRepository> mock = new Mock<IPayrollRepository>();

            mock.Setup(m => m.Departments).Returns(new Department[] {
            new Department {Id = 1, Name = "D1"},
            dep,
            new Department {Id = 3, Name = "D3"},
            }.AsQueryable());


            DepartmentController target = new DepartmentController(mock.Object);

            target.DeleteConfirmed(dep.Id);

            mock.Verify(m => m.DeleteDepartment(dep.Id));
        }
    }
}
