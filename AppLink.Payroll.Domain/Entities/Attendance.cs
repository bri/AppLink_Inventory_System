﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLink.Payroll.Domain.Entities
{
    public class Attendance
    {
        public int Id { get; set; }
        public DateTime TimeIn { get; set; }
        public DateTime TimeOut { get; set; }

        public float TotalHours { get; set; }

        public string EmployeeId { get; set; }

        public Employee Employee { get; set; }
        //public string UserId { get; set; }

        //public ApplicationUser User { get; set; }
    }



}
