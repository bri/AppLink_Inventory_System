﻿using AppLink.Payroll.Domain.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLink.Payroll.Domain.Entities
{
   public class Employee 
    {
        public int Id { get; set; }

        public string EmployeeNumber { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set;    }
        public string LastName { get; set; }

        public string Email { get; set; }
        public string Mobile { get; set; }
        public string WorkPhone { get; set; }
        public DateTime  Birthday { get; set; }
        public string CreatedBy { get; set; }
  
        public bool IsActive { get; set; }
        public string UserId { get; set; }

        public string MailingAddress1 { get; set; }
        public string MailingAddress2 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public DateTime DateHired { get; set; }
        public DateTime DateCreated { get; set; }

        //public int PositionId { get; set; }
        //public Position Position { get; set; }
        public ApplicationUser User { get; set; }
        //public EmployeeDepartmentPosition EmployeeDepartmentPosition { get; set; }
        //public virtual ICollection<Department> Departments { get; set; }
        //public virtual ICollection<EmployeeDepartmentPosition> EmployeeDepartmentPosition { get; set; }


    }

    public class Department
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string  Description { get; set; }

        public string CreatedBy { get; set; }

        //public ApplicationUser User { get; set; }
        //public virtual ICollection<Employee> Employees { get; set; }
        //public virtual ICollection<Position> Positions { get; set; }
        //public virtual ICollection<EmployeeDepartmentPosition> EmployeeDepartmentPositions { get; set; }

    }

    public class Position
    {

        public int Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public string CreatedBy { get; set; }
        public int DepartmentId { get; set; }
        //public virtual ICollection<Department> Departments { get; set; }
        public virtual Department Department { get; set; }
        //public virtual ICollection<Employee> Employees { get; set; }

        //public virtual ICollection<EmployeeDepartmentPosition> EmployeeDepartmentPosition { get; set; }

    }

    public class EmployeeDepartmentPosition
        {[Key,Column(Order=0)]
        public int EmployeeId { get; set; }
        [Key, Column(Order = 1)]
        public int DepartmentId { get; set; }

        [Key, Column(Order = 2)]
        public int PositionId { get; set; }

        public DateTime DateAdded { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        [ForeignKey("DepartmentId")]
        public virtual Department Department { get; set; }
        [ForeignKey("PositionId")]
        public virtual Position Position { get; set; }


    
    }
   
}
