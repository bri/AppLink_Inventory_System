﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLink.Payroll.Domain.Entities
{
   public class Company
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Details { get; set; }

        public string UserId { get; set; }

        public ApplicationUser User { get; set; }
    }


    public class Inventory
    {
        public int Id { get; set; }

    }
}
