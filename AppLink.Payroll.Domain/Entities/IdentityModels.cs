﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AppLink.Payroll.Domain.Entities
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }



        public DbSet<Company> Companies { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<EmployeeDepartmentPosition> EmployeeDepartmentPositions { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<EmployeeDepartmentPosition>().
            //    HasRequired(d => d.Employee).
            //    WithMany(d => d.).
            //    HasForeignKey(d => d.EmployeeId)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Position>().
            //   HasRequired(d=> d.Employees).
            //   WithMany(d=> d.)

            //modelBuilder.Entity<Position>().
            //    HasRequired(r => r.Employees).
            //    WithMany().
            //    WillCascadeOnDelete(false);

            //modelBuilder.Entity<Department>().
            //    HasRequired(r => r.Employees).
            //    WithMany().
            //    WillCascadeOnDelete(false);

            //modelBuilder.Entity<EmployeeDepartmentPosition>().
            //    HasRequired(r => r.Department).
            //    WithMany().
            //    WillCascadeOnDelete(false);

            modelBuilder.Entity<EmployeeDepartmentPosition>().
                HasRequired(r => r.Employee).
                WithMany().
                WillCascadeOnDelete(false);

      
            modelBuilder.Entity<EmployeeDepartmentPosition>().
             HasRequired(r => r.Position).
             WithMany().
             WillCascadeOnDelete(false);


            base.OnModelCreating(modelBuilder);
        }
    }

   
}