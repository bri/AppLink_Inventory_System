﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLink.Payroll.Domain.Entities
{
    public class PaySlip
    {
        public int Id { get; set; }
        public float Salary { get; set; }
        [DataType(DataType.Date)]
        public DateTime TimeOut { get; set; }
        public DateTime TimeIn { get; set; }
        public float TotalHours { get; set; }
        public string EmployeeId { get; set; }

        public Employee Employee { get; set; }
    }



}
