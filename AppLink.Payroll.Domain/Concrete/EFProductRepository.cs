﻿using AppLink.Payroll.Domain.Abstract;
using AppLink.Payroll.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLink.Payroll.Domain.Concrete
{
   public class EFProductRepository:IPayrollRepository
    {
        private ApplicationDbContext context = new ApplicationDbContext();

        public IQueryable<Department> Departments
        {
            get
            {
             return   context.Departments;
            }
        }

        //public IQueryable<Product> Products
        //{
        //    get { return context.Products; }
        //}

        public void SaveDepartment(Department department)
        {
            if (department.Id == 0)
            {
                context.Departments.Add(department);
            }else
            {
                Department newDep = context.Departments.Find(department.Id);
                if (newDep != null)
                {
                    newDep.Name = department.Name;
                    newDep.Description = department.Description;
                    newDep.CreatedBy = department.CreatedBy;
           
                }
            }

            context.SaveChanges();
         
        }


        public Department DeleteDepartment(int depId)
        {
            Department dbEntry = context.Departments.Find(depId);
            if (dbEntry != null)
            {
                context.Departments .Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
        
        public void SaveEmployee(Employee emp)
        {
            if (emp.Id == 0)
            {
                context.Employees.Add(emp);
            }
            else
            {
                Employee  newEmp = context.Employees.Find(emp.Id);
                if (newEmp != null)
                {
                    newEmp.FirstName = emp.FirstName;
                    newEmp.MiddleName = emp.MiddleName;
                    newEmp.LastName = emp.LastName;
                    newEmp.MailingAddress1 = emp.MailingAddress1;
                    newEmp.MailingAddress2 = emp.MailingAddress2;
                    newEmp.IsActive = emp.IsActive;
                    newEmp.Mobile = emp.Mobile;
                    newEmp.WorkPhone = emp.WorkPhone;
                    newEmp.ZipCode = emp.ZipCode;
                   

                }
            }

            context.SaveChanges();
        }

        public float ComputeSalary(float ratePerHour, float numOfHours)
        {
            return ratePerHour * numOfHours; 
        }
    }
}
