﻿using AppLink.Payroll.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLink.Payroll.Domain.Abstract
{
    public interface IPayrollRepository
    {
        //IQueryable<Product> Products { get; }
        IQueryable<Department> Departments { get; }

        void SaveEmployee(Employee emp);
        void SaveDepartment(Department department);
        Department DeleteDepartment(int depId);


        float ComputeSalary(float ratePerHour, float numOfHours);
    }
}
